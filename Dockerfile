FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

# hadolint ignore=DL3018
RUN apk --no-cache add helm kubectl jq bash

COPY plugin.yaml /home/argocd/cmp-server/config/plugin.yaml
COPY --chmod=0755 argocd-helm-plugin.sh /usr/bin/argocd-helm-plugin
