#!/bin/bash

set -euo pipefail

# log info to stderr
log_info() {
  echo 1>&2 "$@"
}

log_info "ARGOCD_APP_PARAMETERS: $ARGOCD_APP_PARAMETERS"

# fetch secret values from managing k8s cluster
SECRET_VALUES=""
if [ -n "${PARAM_VALUES_SECRET_REF:-}" ] ; then
  IFS=/ read -r namespace name key <<<"$PARAM_VALUES_SECRET_REF"
  sa=/var/run/secrets/helm-plugin-serviceaccount

  normalized_key=$(sed 's/\./\\./g' <<<"$key")
  SECRET_VALUES=$(
    kubectl \
      --server=https://kubernetes.default.svc \
      --token="$(cat "$sa/token")" \
      --certificate-authority="$sa/ca.crt" \
      get secret -n "$namespace" "$name" -o jsonpath="{.data['${normalized_key}']}" | base64 -d
  )
fi

helm_args=( )

# generate -f arguments (makes sure filenames can contain spaces)
while IFS=$'\n' read -r file ; do
  helm_args+=("-f" "$file")
done < <(jq -r '.[] | select(.name == "values-files").array[]' <<<"$ARGOCD_APP_PARAMETERS")

# generate --set arguments
for key in $(jq -r '.[] | select(.name == "set-values").map | keys[]' <<<"$ARGOCD_APP_PARAMETERS") ; do
  helm_args+=("--set" "$key=$(echo "$ARGOCD_APP_PARAMETERS" | jq --arg key "$key" -r '.[] | select(.name == "set-values").map[$key]')")
done

log_info "additional helm args: " "${helm_args[@]}"

helm template \
  --namespace "$ARGOCD_APP_NAMESPACE" \
  -f <(echo "${PARAM_VALUES:-}") \
  -f <(echo "${SECRET_VALUES}") \
  --include-crds \
  "${PARAM_RELEASE_NAME:-$ARGOCD_APP_NAME}" \
  "${helm_args[@]}" \
  . 
