# ArgoCD Helm Plugin

This is a Helm plugin for ArgoCD. Although ArgoCD supports Helm natively, it unfortunately doesn't support loading values from a secret. This plugin adds this functionality.

## Installation

### Patch argocd-repo-server deployment

```yaml
---
spec:
  template:
    spec:
      volumes:
        - secret:
            secretName: helm-plugin-token
          name: helm-plugin-token
        - emptyDir: {}
          name: helm-plugin-tmp
      containers:
        - name: helm-plugin
          image: index.docker.io/hojerst/argocd-helm-plugin:v1.1.3 # available versions see release page
          command: [/var/run/argocd/argocd-cmp-server]
          securityContext:
            runAsNonRoot: true
            runAsUser: 999
          volumeMounts:
            - mountPath: /var/run/argocd
              name: var-files
            - mountPath: /home/argocd/cmp-server/plugins
              name: plugins
            - mountPath: /tmp
              name: helm-plugin-tmp
            - mountPath: /var/run/secrets/helm-plugin-serviceaccount
              name: helm-plugin-token
```

### Create a Service Account for the plugin and allow it to read secrets

Note: The following Role allows to access secrets in the ArgoCD namespace only. However, you can modify the Role to allow access to secrets in other namespaces as well. (i.e. use a ClusterRole / ClusterRoleBinding instead)

```yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: helm-plugin
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: helm-plugin
rules:
  - apiGroups: [""]
    resources: ["secrets"]
    verbs: ["get", "list"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: helm-plugin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: helm-plugin
subjects:
  - kind: ServiceAccount
    name: helm-plugin
---
apiVersion: v1
kind: Secret
metadata:
  name: helm-plugin-token
  annotations:
    kubernetes.io/service-account.name: helm-plugin
type: kubernetes.io/service-account-token
```

## Usage

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: my-app
  namespace: argocd
spec:
  destination:
    name: in-cluster
    namespace: demo
  source:
    chart: podinfo
    repoURL: https://stefanprodan.github.io/podinfo
    targetRevision: 6.6.2
    plugin:
      name: helm-v1
      parameters:
        - name: release-name
          string: my-release
        - name: values-secret-ref
          string: argocd/my-secret/values.yaml
        #- name: set-values
        #  map:
        #    key: value
        #    key.subkey: value2
        #- name: values-files
        #  array: ["file1.yaml", "file2.yaml]
        - name: values
          string: |
            ui:
              color: "#34577c"
---
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
  namespace: argocd
stringData:
  values.yaml: |
    ui:
      message: "my secret message"
```

Note: You can also manage the actual secret with something like [SealedSecrets](https://sealed-secrets.netlify.app/) or [ExternalSecrets](https://external-secrets.io).
